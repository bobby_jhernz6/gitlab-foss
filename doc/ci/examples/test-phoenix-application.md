---
redirect_to: 'README.md'
---

This example is no longer available. [View other examples](README.md).

<!-- This redirect file can be deleted after February 1, 2021. -->
<!-- Before deletion, see: https://docs.gitlab.com/ee/development/documentation/#move-or-rename-a-page -->
